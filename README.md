# Summary

This codebase provides docker container template for an application wrapped in docker container. 
It also comes with scripts to install and update current installation in your application. The main goals of this
bootstrap template is to provide easier way to build dockerized applications, based on pre-built snippets.

There are plenty of features in gives, such as:

- join multiple dockerized applications into cluster, let them connect to each other
    - *but you still can control what service can connect to what application*
- bunch of snippets to use in Dockerfile images, for instance to install XDebug `bash /snippets/install-composer.sh ${PHP_VERSION}`
- separate dev / prod build and installation
    - it is flexible as hell, for example you can override everything on instance level, or on environment level. 
      or just on build step level, or even on build level when environment = dev
- set network per application, like `192.168.200.x`, where `x` is # of service (numbers start from `2`) 

Features Docker does not provide natively:

- simple way to run data / scheme migrations
- MacOS network driver
- run cron jobs on any service
- generate host-wide SSL certificate


# Quick-start Guide

*Note: If you have already installed bootstrap to your application you may need to update the docker dependency. 
It's covered in the next section*

The quickest and fastest way to start installation is to 
### 1) Run the following command:

```shell
docker run -it --rm -v $(realpath .):/app bitnami/git bash -c "curl -sL https://bit.ly/docker-foundation-install | bash"
```

`$(realpath .)` may be replaced with absolute path to your application, or `.` could be replaced with relative path

This command produces output [like this](https://files.slack.com/files-pri/T52JCC6GL-F03M94RA95E/image.png?pub_secret=9648a240ab) 
with information what's next steps may be taken next (even with suggested order of the steps). 

You can even use this command to an application that already exists, but is not dockerized yet. 
The only requirement is it should not be already dockerized (do not have `docker-compose.yml` configuration).

Alternatively you can copy files this repository to your application, this is what installer does. 
Though, additional steps the installers does are:

- copy `.env.yml.example` to `.env.yml`
- copy `.env.example` to `.env` and merge with existing if any
- copy `.gitignore` and merge with existing if any
- copy `README-TEMPLATE.md` to `README.md`


### 2) Define list of services in `docker-compose.yml`
You may need or not need some services, like db


### 3) Configure `.env` parameters as you wish, especially the following variables:

- `COMPOSE_PROJECT_NAME` - project name, should be unique, otherwise there will be network and volumes issues
- `CONTAINER_ENVIRONMENT` - `dev` or `prod`, depends on what environment do you need
    - `dev` equals `prod` and few more packages over it:
        - php-xdebug - useful php module to debug the application
        - vim (to edit / see files in container)
        - ping (to ping something from container)
- `CONTAINER_SUBNET` - `192.168.x`, where `x` is unique number for your application
    - for instance for Proxy Backend it's 200, WHMCS - 201, etc
    - to find out what's number is not occupied you can use [this search in BitBucket](https://bitbucket.org/search?q=CONTAINER_SUBNET%3D%20ext%3Aexample)
    - subnet is used for bridge network. services in docker are numerated starting from 2,
      ie if subnet is `192.168.200`, **app1** is `192.168.200.2`, **db1** is `192.168.200.3`, etc
- `CONTAINER_DB1_ROOT_PWD` - MySQL root password (remove if db is not needed)
- `CONTAINER_DB1_PWD` - MySQL user password, with access granted to the application database (remove if db is not needed)
- `CONTAINER_DOMAIN_*` - domains for you application services
- `CONTAINER_SSL_*` - useful for localhost development, to generate valid root SSL use this command `docker/ssl-generate.sh`

**App-specific `.env` parameters:**

*refer to the application documentation*


**Other `.env` parameters might be useful to edit:**

- `CONTAINER_XDEBUG_PORT` - xdebug port, that should be configured in IDE additionally

### 4) `deploy.sh`

This file defines what steps are needed to build your `app1` main application. This framework assumes at least 1 service
is to be built, and name for the service is `app1`. Typical example is:

```shell
composer install -n # load dependencies
  && composer dump -n # persist classes paths
```

### 5) Build images at `docker-build/image/`

`app1` image path is defined at `docker-compose.yml`

```yaml
services:
  app1:
    build:
      dockerfile: docker-build/image/php-apache-cron.dockerfile
```

You may add another image, or modify the default one - up to you and / or application needs. Images are in Dockerfile format, 
more information on this format may be found at [official documentation](https://docs.docker.com/engine/reference/builder/).


### 6) Services configs `docker-build/config/`

The files are included in images, for example php.ini is included like this:

```dockerfile
COPY docker-build/config/php/php.ini /usr/local/etc/php/
```

Note that apache config is loaded by default from `.docker-foundation/` directory:

```dockerfile
COPY .docker-foundation/config/apache2/default.conf /etc/apache2/sites-enabled/
```

therefore, if you need to customize apache config you should copy `.docker-foundation/config/apache2/default.conf` to 
`docker-build/config/apache2/default.conf` and update you image (Dockerfile) accordingly.

### 7) Set up post-build steps in `docker-build/build.d/post-build/`

See examples there how these steps are made, and feel free to remove / add new steps. To understand why / when these
steps are being run you should see what workflow is of the build process:

1. Load `CONTAINER_SSL_ROOT_*` keys from physical paths
2. Run pre-build steps from `docker-build/build.d/before/`
3. Build images (from step 5)
4. Run in special "build" mode
   - this mode uses `docker-compose-build.yml` / `docker-compose-build-dev.yml` configs that, by default, 
     do not run cron jobs and links .container-outworld directory to cache built dependencies outside the container 
5. Run post-build steps
6. Run container in normal mode (separate step, run by `docker/start.sh` or `docker/dev-start.sh` script)

### 8) (optional) Add cron jobs to `docker-build/cron.d/`

### 9) Update `README.md`

Sections you may want to fill:

- List of commands to deploy application on a new environment
- Define known environments (do not forget to keep updates to this section to help other developers)
- Add known issues upon deployment


When this all developed and configured to build the contaiener with its services. To do so:
```shell
bash docker/build.sh
```

And then start it
```shell
bash docker/start.sh
```
or for dev features
```shell
bash docker/dev-start.sh
```

Dev features mainly consist of syncing changes outside of container to container. 
So basically you can edit code in IDE and see immediate changes in application in container.

### How dev environment differs from production?

Essentially dev = prod + some extra features.

On build stage it includes extra packages (see more about it in `CONTAINER_ENVIRONMENT` description).

When you start the application script merges the following configs:

- For prod
    - docker-compose.yml
    - .env.yml
- For dev
    - docker-compose.yml
    - docker-compose-dev.yml
    - .env.yml

`docker-composer.yml` is base config for builder, and others extend it.

`docker-compose-dev.yml` provides extra features like edit apache and php configs on-fly (rebuild of container is not needed).
Other directives may be defined there, for example, to use different image for dev build.

`.env.yml` is config for current environment that allows to override underlying configs. It's not coming to repository, 
so you may override anything there and keep it local.

# Quick-update of existing framework Guide

To do so please use the following command in context of the app directory**
```shell
docker run -it --rm -v $(realpath .):/app bitnami/git bash -c "curl -sL https://bit.ly/docker-foundation-update | bash"
```

`$(realpath .)` may be replaced with absolute path to your application, or `.` could be replaced with relative path

# What are commands in `docker/` do?

- `docker/ssl-generate.sh` - generate / get root SSL and outputs snippet to be added to `.env` file
- `docker/build.sh` - run build operation (**IMPORTANT**: after build either `docker/start.sh` or `dev-start.sh` should be executed to finalize)
- `docker/dev-start.sh` - run container in dev mode
- `docker/start.sh` - run container
- `docker/shell.sh <service_name> <command>` - run a service's shell, parameters:
    - `<service_name>` - service name from `docker-compose.yml`, default: `app1`
    - `command` - command to be run, default: `bash` (what runs interactive bash session), other example: `docker/shell.sh app1 ls -l`
- `docker/stop.sh` - does what it says, stops container and its services (`app1`, `db1`, any other defined in `docker-compose.yml`)

To make the usage even more comfortable you can use the folliwng aliases:
- `dcb` - билд контейнера
- `dcp` - запуск в production режиме (обычный режим, изменения сорса не попадают в сервис контейнера)
- `dcu` - запуск в dev режиме (изменения в сорсе попадают в сервис контейнера
- `dcd` - остановка сервисов контейнера
- `dex {app name} {command}` - запуск консоли запущенного приложения, аргументы
  - `{app name}` - имя сервиса контейнера, опционально, по умолчанию - app1
  - `{command}` - команда для запуска в сервисе контейнера, опционально, по умолчанию bash
- `dfin` - загрузка шаблона проекта из docker bootstrap репозитория - удобно в начале проекта
- `dfup` - обновление файлов библиотеки из docker bootstrap - в процессе работы над проектом, если появился апдейт в docker bootstrap

```bash
sed -i '/######## snippet-start ########/,/######## snippet-end ########/d' ~/.bashrc \
&& cat <<-END >> ~/.bashrc && source ~/.bashrc
######## snippet-start ########
########
# Docker shortcuts
########
function dex-fn {
 container_id="\$1"

 # Modern case, use shell.sh
 if [ -f docker/shell.sh ]; then
 bash docker/shell.sh "\$@"

 # Parse name of container from .env (legacy approach)
 elif [ -z "\$container_id" ] && [ -f .env ]; then
 project_id=\$(grep -oE "^COMPOSE_PROJECT_NAME=.*" .env | sed -E 's/^[^=]+=//g')
 container_id="\$project_id"_app1_1

 sudo docker exec -it \$container_id \${2:-bash}

 # Use dex just as a shortcut
 else
 sudo docker exec -it \$container_id \${2:-bash}
 fi
}

function dcu-fn {
 if [ -f docker/dev-start.sh ]; then
 bash docker/dev-start.sh
 else
 sudo docker-compose up -d
 fi
}

function dcd-fn {
 if [ -f docker/stop.sh ]; then
 bash docker/stop.sh
 else
 sudo docker-compose down
 fi
}

# Composer shortcuts
alias dex=dex-fn # shell.sh
alias dcu=dcu-fn # dev-start.sh
alias dcb="bash docker/build.sh"
alias dcp="bash docker/start.sh"
alias dcd=dcd-fn # down.sh

# Foundation
alias dfin='docker run -it --rm -v \$(realpath .):/app bitnami/git bash -c "curl -sL https://bit.ly/docker-foundation-install | bash"'
alias dfup='docker run -it --rm -v \$(realpath .):/app bitnami/git bash -c "curl -sL https://bit.ly/docker-foundation-update | bash"'

# Docker shortcuts
alias drun="docker run -it --rm"

######## snippet-end ########
END
```

# How can I use applications / services

## Linux / MacOS

*Works on MacOS similarly to Linux thanks by tun/tap driver built-in to this framework*

You can access applications / service by IP (see `CONTAINER_SUBNET` description for more details). 
However, I recommend adding the IPs to [/etc/hosts](https://linuxize.com/post/how-to-edit-your-hosts-file/) so every application / service has own domain.

Example of /etc/hosts

```text
# Docker domains
192.168.200.2 test.proxy.rayobyte.tld
192.168.200.3 db.test.proxy.rayobyte.tld
192.168.200.4 pma.test.proxy.rayobyte.tld
```

### Connection between applications / containers

By default, every `app1` service is being added to `sprious-apps` network. Below you may see it's set in every `.env` by that default.

```yaml
version: '3.7'
services:
    app1:
        networks:
            sprious: # network connection
networks:
    sprious: # network definition
        external: true
        name: sprious-apps
```

## Windows (not tested yet)

You can use [this solution](https://forums.docker.com/t/bridge-with-docker-for-windows/30936/5) and configure domain per application / service, although it's not tested yet and not confirmed to work.

## Use native docker port technique

By this option you have to share ports per service, 
and then you can set port forwarding from container to localhost (`127.0.0.1`). To do so, add the following to `.env.yml` 
and start container again:

```yaml
services:
    app1:
        ...
        ports:
            - "10001:80"
```
where:

- `app1` - service name (see them all defined in `docker-compose.yml`)
- `80` - port inside the container you want to forward (for MySQL it's 3006)
- `10001` - port to be attached to

Example of nginx config, though for **Valet** please skip this config 
and see the following section about Valet + Nginx settings

```shell
server{
    listen 80;
    listen 443 ssl;

    ssl_certificate /ssl/fullchain.crt;
    ssl_certificate_key /ssl/privkey.key;

    server_name test.proxy.rayobyte.tld

    location / {
        proxy_pass http://127.0.0.1:10001/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Where 10001 is http port that's forwarded from container

sudo sh -c 'echo "127.0.0.1 test.proxy.rayobyte.tld" >> /etc/hosts'

### Valet config

Let's say

```shell
valet link backend.proxy.rayobyte \
&& valet secure test.proxy.rayobyte \
&& sudo sh -c 'echo "127.0.0.1 test.proxy.rayobyte.hz" >> /etc/hosts'
&& nano ~/.config/valet/Nginx/test.proxy.rayobyte.hz
```

where edit the config to make like:

```text
server {
    listen 127.0.0.1:443 ssl http2;
    
    ...

    #location / {
    #   rewrite ^ "/Users/your-user/.composer/vendor/laravel/valet/server.php" last;
    #}
    
    #location ~ [^/]\.php(/|$) {
    #    ...
    #}

    ...

    location / {
        proxy_pass http://127.0.0.1:10001/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    #location ~ /\.ht {
    #    deny all;
    #}
}
```

Note `10001` is http port that's forwarded from container

### Connection between applications / containers

There is a restriction with Nginx / Valet solution when you need to connect
one application to another. To work around it, please merge the following to `.env.yml`
```yaml
services:
    app1:
        extra_hosts:
            - "yourdomain.host:host-gateway"
```
where `yourdomain.host` is domain you need to connect to (like Proxy Dashboard connects to `backend.proxy.rayobyte.tld`)


# SSL configuration

The process as a whole consist of the few steps:

1. Issue root certificate    
2. Issue domain certificate, that's signed by root certificate
    - built automatically, and stored in `docker-bootstrap/docker-build/persistent/ssl`
3. (optional) Use domain certificate to add to your local nginx setup

To issue the root certificate (step #1) the only command you should execute is:

```shell
bash docker/lib/util/ssl-issue-root.sh
```

After this, the script stores generated certificates in local directory 
and outputs information how this path can be used by container *(hint: update ssl directives in .env)*


#### Import to Chrome

1. Open [certificates](vivaldi://settings/certificates) in Chrome
2. Go to **Authorities** tab
3. Click **Import**
4. Select certificate from script output for `CONTAINER_SSL_ROOT_PUBLIC` (.crt extension)


#### Import to OS

Use [this good guide](https://manuals.gfi.com/en/kerio/connect/content/server-configuration/ssl-certificates/adding-trusted-root-certificates-to-the-server-1605.html) to add certificate system-wide 


## Alternative - [mkcert](https://github.com/FiloSottile/mkcert) tool

```shell
bash .docker-foundation/bin/util/ssl-mkcert-root.sh
```

After this, the script stores generated certificates in local directory
and outputs information how this path can beused by container (hint: update ssl directives in .env)

Pros of this way:
- root certificate is installed to system / browser by this tool, no manual actions are needed

Cons of this way:
- takes some time to install to the system, while it's to be used just once


# Xdebug guide

To use xdebug, set env variables:

- `CONTAINER_ENVIRONMENT` to `dev`
- `CONTAINER_XDEBUG_PORT` to some unique value for this project (should be different from other projects)

Set xdebug port to your IDE to `CONTAINER_XDEBUG_PORT` value. For Webstorm / PHPStorm it's here:
![](https://files.slack.com/files-pri/T52JCC6GL-F03H26Z2G7L/image.png?pub_secret=3a5bb6d4bd)

Start listening Xdebug connections in IDE. For Webstorm / PHPStorm it's here:
![](https://files.slack.com/files-pri/T52JCC6GL-F03GMK81EK1/image.png?pub_secret=94e34803cf)

Then build + start the container

```shell
bash docker/build.sh && bash docker/dev-start.sh
```

Now you have 2 options to debug:

**From browser:**

Open domain / site you need to debug and put the following to address bar:
```javascript
javascript:(/** @version 0.4.1 */function() {document.cookie = "XDEBUG_SESSION=xdebug; path=/;";})()
```

Every request you make now triggers Xdebug in IDE. To turn it off:
```javascript
javascript:(/** @version 0.4.1 */function() {document.cookie = "XDEBUG_SESSION=; expires=Mon, 05 Jul 2000 00:00:00 GMT;path=/;";})()
```

To simplify the process of turning on / off, you can save the scripts above to bookmarks and then just click them :)

![](https://files.slack.com/files-pri/T52JCC6GL-F03H4HUTL4C/image.png?pub_secret=7d435eec25)

**From console:**

```shell
php-xdebug your-script.php
```

# Known issues


### *Error response from daemon: Invalid address 192.168.221.3: It does not belong to any of this network's subnets*

Solution:
1. Revert `CONTAINER_SUBNET` to what it was earlier
2. `docker/stop.sh`
3. Set `CONTAINER_SUBNET` to what you need
4. Start the container with `docker/dev-start.sh` or `docker/start.sh`


### *Network sprious-apps declared as external, but could not be found*

Do what it says to do

```shell
docker network create sprious-apps
```

and then rerun the previous command you tried to execute