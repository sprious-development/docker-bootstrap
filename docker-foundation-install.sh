#!/bin/bash

# Stop on exception
set -e

if [ ! -d /app/ ]; then
  echo '[ERROR] No directory attached to /app, make sure you added "-v \$(realpath .):/app" parameter to command'
fi

formatError="\033[0;31m"
formatSuccess="\033[1;32m"
formatInfo="\033[1;36m"
formatGray="\033[0;37m"
formatDefault="\033[0m"

if [ -f /app/docker-compose.yml ]; then
  echo -en $formatError
  echo '[ERROR] Can not install, app directory already contains "docker-compose.yml"'
  echo -en $formatDefault
  echo
  echo 'Make sure you install template to an app that is not wrapped up by docker container template'
  echo 'If you wish to update existing app with docker container, please use the following command:'
  echo -en $formatInfo
  echo 'docker run -it --rm -v $(realpath .):/app bitnami/git bash -c "curl -sL https://bit.ly/docker-foundation-update | bash"'
  echo
  echo -en $formatGray
  echo "Directory index:"
  ls -la /app
  echo -en $formatDefault

  exit 1
fi

echo
echo "Loading dependency ..."
echo -en $formatGray
git clone https://bitbucket.org/sprious-development/docker-bootstrap.git
echo -en $formatDefault

echo
echo "Installing ..."
echo -en $formatInfo
echo /
echo -en $formatDefault
for i in $( cat <<-END
.docker-foundation/
docker/
docker-build/
.env.yml.example
.dockerignore
bitbucket-pipelines.yml
deploy.sh
docker-compose.yml
docker-compose-build.yml
docker-compose-build-dev.yml
docker-compose-dev.yml
END
)
do
  echo -en $formatInfo
  echo "| - $i"
  echo -en $formatDefault
  cp -r docker-bootstrap/$i /app/
  chmod -R ugo+r /app/$i
done

echo
echo "Preparing ..."

if [ ! -f /app/.env.example ]; then
  cp docker-bootstrap/.env.example /app
  chmod -R ugo+r /app/.env.example
else
  echo Merging .env.example...
  echo \ >> /app/.env.example
  echo \ >> /app/.env.example
  cat docker-bootstrap/.env.example >> /app/.env.example
fi

if [ ! -f /app/.env ]; then
  cp docker-bootstrap/.env.example /app/.env
  chmod -R ugo+r /app/.env
else
  echo Merging .env...
  echo \ >> /app/.env
  echo \ >> /app/.env
  cat docker-bootstrap/.env.example >> /app/.env
fi

cp -p /app/.env.yml.example /app/.env.yml

if [ ! -f /app/README.md ]; then
  cp docker-bootstrap/README-TEMPLATE.md /app/README.md
  chmod -R ugo+r /app/README.md
else
  echo Merging README.md...
  echo \ >> /app/README.md
  echo \ >> /app/README.md
  cat docker-bootstrap/README-TEMPLATE.md >> /app/README.md
fi

if [ ! -f /app/.gitignore ]; then
  cp docker-bootstrap/.gitignore /app/
  chmod -R ugo+r /app/.gitignore
else
  echo Merging .gitignore...
  echo \ >> /app/.gitignore
  echo \ >> /app/.gitignore
  cat docker-bootstrap/.gitignore >> /app/.gitignore
fi

echo -en $formatSuccess
echo
echo [Success]
echo -en $formatDefault
echo

echo "Now, you can configure the following:"
echo -e "${formatInfo}/"
echo -e "| - docker-compose.yml ${formatDefault} # define application services ${formatInfo}"
echo -e "| - .env ${formatDefault} # set project name, subnet, domains ${formatInfo}"
echo -e "| - docker/ssl-generate.sh ${formatDefault} # generate SSL certificates (convenient / necessary on localhost) ${formatInfo}"
echo -e "| - deploy.sh ${formatDefault} # define steps necessary to deploy app1 ${formatInfo}"
echo -e "| - docker-build/image/ ${formatDefault} # develop or improve Dockerfile image ${formatInfo}"
echo -e "| - docker-build/config/ ${formatDefault} # develop or improve configs for services ${formatInfo}"
echo -e "| - docker-build/build.d/post-build/ ${formatDefault} # define / remove post build steps (db migration, set permissions, etc) ${formatInfo}"
echo -e "| - docker-build/cron.d/ ${formatDefault} # set cron jobs for services ${formatInfo}"
echo -e "| - README.md ${formatDefault} # read and update README template ${formatInfo}"
echo -en $formatDefault
echo