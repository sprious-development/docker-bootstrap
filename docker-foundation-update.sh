#!/bin/bash

# Stop on exception
set -e

formatError="\033[0;31m"
formatSuccess="\033[1;32m"
formatInfo="\033[1;36m"
formatGray="\033[0;37m"
formatDefault="\033[0m"

if [ ! -d /app/.docker-foundation ]; then
  echo -en $formatError
  echo "[ERROR] No ./.docker-foundation directory exists, can not update the project"
  echo -en $formatDefault
  if [ -d /app ]; then
    echo
    echo -en $formatGray
    echo "Directory index:"
    ls -la /app
    echo -en $formatDefault
  fi
  exit 1
fi

echo
echo "Loading dependency ..."
echo -en $formatGray
git clone https://bitbucket.org/sprious-development/docker-bootstrap.git
echo -en $formatDefault

echo
echo "Updating .docker-foundation/..."
cp -r docker-bootstrap/.docker-foundation /app/

echo -en $formatSuccess
echo
echo [Success]
echo -en $formatDefault
echo