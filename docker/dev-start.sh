#!/bin/bash

# This script is kept in docker/ directory just for convenience
# and compatibility sake, original scripts are in .docker-foundation/bin/

dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
bash $dir/../.docker-foundation/bin/dev-start.sh "$@"