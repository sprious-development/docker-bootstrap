ARG PHP_VERSION

FROM php:${PHP_VERSION}-apache AS base
WORKDIR /app

# Share snippets / secrets
COPY .docker-foundation/image-snippets /snippets
COPY docker-build/secrets /secrets

# Configure system
RUN bash /snippets/adjust-terminal-colors.sh \
  && apt-get update && apt-get install -y --no-install-recommends \
    # timezone
    tzdata \
    # docker/build.sh - sync deps
    rsync \
    # debug utils
    vim iputils-ping traceroute telnet less curl \
    # docker/build.sh - db migration
    netcat

# Generate SSL certificate
ARG DOMAIN
RUN bash /snippets/generate-ssl.sh ${DOMAIN}

# Configure Apache
ARG HTTP_ROOT
COPY .docker-foundation/config/apache2/default.conf /etc/apache2/sites-enabled/
RUN bash /snippets/apache-default-configuration.sh ${HTTP_ROOT:-/}

# Configure PHP
COPY docker-build/config/php/php.ini /usr/local/etc/php/
RUN apt-get install -y --no-install-recommends libc-client-dev libxml2-dev libzip-dev libonig-dev \
	# Common
	&& docker-php-ext-install mbstring iconv \
	# DB
	&& docker-php-ext-install pdo pdo_mysql \
	# optional dependencies (works on 5.6 crashes on newer)
	&& apt-get install -y --no-install-recommends libmcrypt-dev && docker-php-ext-install mcrypt || true \
	&& docker-php-ext-install mysql || true \
	# GD
	&& apt-get install -y --no-install-recommends libpng-dev libfreetype6-dev libjpeg62-turbo-dev \
	&& docker-php-ext-install gd \

	# After new modules added load it to httpd
	&& service apache2 restart

# Install package managers
ARG PHP_VERSION
ENV COMPOSER_HOME="/tmp/composer"
RUN \
    # Composer
    bash /snippets/install-composer.sh ${PHP_VERSION}

# Dev
FROM base AS build_env
ARG ENVIRONMENT
ARG PHP_VERSION
COPY .docker-foundation/config/php/conf.d/ /tmp/php-conf.d
RUN if [ "${ENVIRONMENT}" = "dev" ]; then \
  # XDebug
  bash /snippets/install-xdebug.sh ${PHP_VERSION} \
  ; fi

# Install cron
RUN apt-get install -y --no-install-recommends cron psmisc
COPY ./.docker-foundation/image-bin/cronify /usr/bin/cronify
RUN chmod 500 /usr/bin/cronify
COPY ./docker-build/cron.d/app1/ /etc/cron.d/

# Install MTA (msmtp)
ARG SMTP_HOST
ARG SMTP_PORT
ARG SMTP_DEFAULT_FROM
RUN bash /snippets/install-msmtp-relay.sh "$SMTP_HOST" "$SMTP_DEFAULT_FROM" "$SMTP_PORT"

# Build application
FROM build_env AS build_app
COPY . .
RUN bash deploy.sh "${ENVIRONMENT}" \
	&& find . \
       ! -user www-data \
       ! -name 'vendor*' \
       ! -path './vendor*/*' \
       ! -path './web/assets/vendor*/*' \
       -exec chown www-data {} \;

ARG HTTP_ROOT
ARG HTTP_APP_ROOT
RUN if [ "${HTTP_APP_ROOT}" != "" ]; then \
    # Create symlink
    mkdir -p $( dirname ${HTTP_ROOT}/${HTTP_APP_ROOT} ) && ln -s $(realpath ${HTTP_ROOT}) ${HTTP_ROOT}/${HTTP_APP_ROOT} \
	; fi

# Cleanup / unshare secrets
RUN bash /snippets/install-cleanup.sh && rm -rf /snippets /secrets

ENTRYPOINT ["cronify", "apache2-foreground", "apache2"]