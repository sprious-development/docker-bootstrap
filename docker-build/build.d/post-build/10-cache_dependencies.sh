#!/bin/bash

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
source $dir/../../../.docker-foundation/bin/lib/common.sh

echo "Caching dependencies..."

# Follow setguid, set group write permissions
rsync_cmd="umask 2002; rsync -rlt --chmod=g=rwx --delete"

cat <<-END | $(get_docker_bin) exec -i $(get_service_name app1) sh

$rsync_cmd vendor/ .container-outworld/vendor/ || true; \
$rsync_cmd composer.lock .container-outworld/composer.lock || true;
$rsync_cmd web/assets/vendor/ .container-outworld/web/assets/vendor/ || true

END