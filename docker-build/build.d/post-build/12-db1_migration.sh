#!/bin/bash

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
source $dir/../../../.docker-foundation/bin/lib/common.sh

echo "#######################################"
echo "Running data migration..."
echo "#######################################"
# Wait until mysql is up
cat << END | $(get_docker_bin) exec -i $(get_service_name app1) sh
timeout 30 sh -c 'until nc -z -v \$0 \$1; do sleep 1; done' db1_mysql 3306
END
# Migrate data
cat << END | $(get_docker_bin) exec -i $(get_service_name app1) bash
set -e

# Run migrations
php artisan migrate

# Add dummy data on fresh install
if [ "\$(php artisan users:count)" = "0" ]; then
    php artisan db:seed
    php artisan key:generate
    php artisan passport:install

    # Persist keys
    cp .env .container-outworld/
    cp storage/oauth-private.key .container-outworld/storage/
    cp storage/oauth-public.key .container-outworld/storage/
fi
END