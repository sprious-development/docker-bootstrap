#!/bin/bash

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
source $dir/../../../.docker-foundation/bin/lib/common.sh

# Update permissions for shared entries
echo "Setting permissions..."
$(get_docker_bin) exec -i $(get_service_name app1) sh <<-HEREDOC
find . \
  ! -user www-data \
  ! -name 'vendor*' \
  ! -path './vendor*/*' \
  -not -path './.container-outworld/*' \
  -exec chown www-data {} \;

  # Sessions volume perms fix
  if [ -d /var/tmp/sessions ]; then
    chmod go+w /var/tmp/sessions
  fi
HEREDOC