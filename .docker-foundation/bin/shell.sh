#!/bin/bash

##
# Usage: shell.sh [service name] [shell name]
# examples:
# bash shell.sh
# bash shell.sh app1
# bash shell.sh db1 sh
##

# Exit when any command fails
set -e

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/lib/common.sh
cd $( realpath_shim $dir/../../ )

set +e
name=$(get_service_name "${1:-app1}")
status=$?
if [ $status -eq 1 ]; then
    get_service_name "${1:-app1}"
    exit 1
fi

set -e
echo -en $formatSuccess
echo "~> $name:"
echo -en $formatDefault
$(get_docker_bin) exec -it $(get_service_name "${1:-app1}") "${2:-bash}" "${@:3:10}"