#!/bin/bash

# Exit when any command fails
set -e

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/lib/common.sh
cd $( realpath_shim $dir/../../ )

$( get_docker_compose_bin ) \
  -f docker-compose.yml \
  -f .env.yml \
  down