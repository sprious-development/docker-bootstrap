#!/bin/bash

formatError="\033[0;31m"
formatSuccess="\033[1;32m"
formatInfo="\033[1;36m"
formatGray="\033[0;37m"
formatDefault="\033[0m"

# Get value from .env
function get_config_value() {
  if [ -z "$1" ]; then
    echo -en $formatError
    echo "Error: config parameter name is empty in get_config_value()"
    echo -en $formatDefault
    return 1
  fi

  echo $(grep -oE "^$1=.*" .env | sed -E 's/^[^=]+=//g')
}

# (config_name, secret_name, skip_empty_config_values)
function share_secret_from_config() {
  if [ -z "$1" ]; then
    echo "Error: copy_secret_from_config() parameter 1 (config parameter name) is empty"
    return 1
  fi

  if [ -z "$2" ]; then
    echo "Error: copy_secret_from_config() parameter 2 (secret name) is empty"
    return 1
  fi

  common_dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)

  secrets_dir="$common_dir/../../../docker-build/secrets"
  mkdir -p $secrets_dir # if called the first time

  # Return if value empty
  source_path=$(get_config_value "$1")
  if [ -z "$source_path" ]; then
    if [ -z "$3" ]; then
      echo "Error: copy_secret_from_config() secret value for \"$1\" is empty"
      return 1
    # Just skip the value
    else
      return 0
    fi
  fi
  source_path=$(realpath_shim "$source_path" )
  target="$common_dir/../../../docker-build/secrets/$2"

  if [ ! -f "$source_path" ] && [ ! -d "$source_path" ] && [ ! -L "$source_path" ]; then
    echo "Error: copy_secret_from_config() file \"$source_path\" does not exist"
    return 1
  fi

  cp $source_path $target
}

function unshare_secrets() {
  common_dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
  secrets_dir="$common_dir/../../../docker-build/secrets"

  # Remove files from secrets directory
  if [ "$(ls $secrets_dir | wc -l)" -gt 0 ]; then
    rm $secrets_dir/*
  fi

  if [ -d "$secrets_dir" ]; then
    rmdir $secrets_dir
  fi
}

function realpath_shim() {
  # Check does not exists
  if [ ! -z "$2" ] \
  && [ ! -d "$1" ] && [ ! -f "$1" ]; then
    echo realpath: "$1": No such file or directory
    return 1
  fi

  if ! command -v realpath &> /dev/null; then
    eval path="$1"
    echo "$path"
  else
    eval path="$1"
    realpath "$path"
  fi
}

function get_os_type() {
  # https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script/18434831
  case $(uname | tr '[:upper:]' '[:lower:]') in
    linux*)
      echo linux
      ;;
    darwin*)
      echo osx
      ;;
    msys*)
      echo windows
      ;;
    *)
      echo unknown
      ;;
  esac
}

# ie: Centos v7
function get_os_dist() {
    # https://unix.stackexchange.com/a/6348

    if [ -f /etc/os-release ]; then
        # freedesktop.org and systemd
        . /etc/os-release
        OS=$NAME
        VER=$VERSION_ID
    elif type lsb_release >/dev/null 2>&1; then
        # linuxbase.org
        OS=$(lsb_release -si)
        VER=$(lsb_release -sr)
    elif [ -f /etc/lsb-release ]; then
        # For some versions of Debian/Ubuntu without lsb_release command
        . /etc/lsb-release
        OS=$DISTRIB_ID
        VER=$DISTRIB_RELEASE
    elif [ -f /etc/debian_version ]; then
        # Older Debian/Ubuntu/etc.
        OS=Debian
        VER=$(cat /etc/debian_version)
    elif [ -f /etc/SuSe-release ]; then
        # Older SuSE/etc.
        ...
    elif [ -f /etc/redhat-release ]; then
        # Older Red Hat, CentOS, etc.
        ...
    else
        # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
        OS=$(uname -s)
        VER=$(uname -r)
    fi

    echo $OS v$VER
}

function check_docker_sudo() {
  # Try to run docker ps to see if we can access docker without sudo
  docker ps > /dev/null 2>&1

  # If docker ps succeeded, docker does not need sudo
  if [ $? -eq 0 ]; then
    return 0
  else
    return 1
  fi
}

function get_docker_bin() {
  # Check if docker needs sudo
  if check_docker_sudo; then
    echo "docker"
  else
    echo "sudo docker"
  fi
}

function get_docker_compose_bin() {
  # Determine the appropriate docker-compose command
  local docker_compose_cmd=""

  if command -v docker &> /dev/null && docker compose version &> /dev/null; then
    docker_compose_cmd="docker compose"
  elif command -v docker-compose &> /dev/null; then
    docker_compose_cmd="docker-compose"
  else
    echo "Error: Neither docker compose nor docker-compose is available."
    exit 1
  fi

  # Check if docker needs sudo
  if check_docker_sudo; then
    echo $docker_compose_cmd
  else
    echo "sudo $docker_compose_cmd"
  fi
}


function get_service_name() {
    if [ -z "$1" ]; then
      echo -en $formatError
      echo "Error: get_service_name() parameter 1 (service name) is empty"
      echo -en $formatDefault
      return 1
    fi

    docker_bin="$(get_docker_bin)"
    app_id=$(get_config_value COMPOSE_PROJECT_NAME)

    match_cmd="$docker_bin ps --no-trunc --filter 'name=$app_id' --format '{{.Names}}' | grep -E '^${app_id}[-_]${1}'"
    found=$( eval "$match_cmd" | wc -l )
    if [ "$found" -ne "1" ]; then
        if [ "$found" -gt "1" ]; then
          echo -en $formatError
          echo "Error: Service name '$1' matches multiple services, please add more characters to match just one"
          echo -en $formatDefault
          echo "Matched:"
          echo -en $formatGray
          eval $match_cmd
          echo -en $formatDefault
        else
          echo "Error: Service '$1' is not up or not defined"
        fi
        echo ""
        echo "Available services are:"
        echo -en $formatGray
        $docker_bin ps --no-trunc --filter "name=$app_id" --format "{{.Names}}" | sed -E "s/^$app_id[-_]|[-_][0-9]+$//g"
        echo -en $formatDefault
        return 1
    fi

    echo $( eval "$match_cmd" )
}

function list_services_names() {
    docker_bin="$(get_docker_bin)"
    app_id=$(get_config_value COMPOSE_PROJECT_NAME)
}