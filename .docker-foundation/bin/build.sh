#!/bin/bash

# Exit when any command fails
set -e

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/lib/common.sh
cd $( realpath_shim $dir/../../ )

# Build and enter to "deploy" mode
echo -en $formatInfo
echo "#######################################"
echo "Building..."
echo "#######################################"
echo -en $formatDefault
echo

share_secret_from_config CONTAINER_SSL_ROOT_PRIVATE ssl-root.key true
share_secret_from_config CONTAINER_SSL_ROOT_PUBLIC ssl-root.crt true

# Run before build scripts
for file in $( ls $dir/../../docker-build/build.d/before-build/*.sh ); do
  bash $file
done

# Run builder
dev_config=
if [[ $(get_config_value CONTAINER_ENVIRONMENT) == "dev" ]]; then
  dev_config="-f docker-compose-build-dev.yml"
fi

# Prepend sudo for systems where it's needed, but also pass env arguments
eval $( get_docker_compose_bin | grep -o sudo ) COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 $( get_docker_compose_bin | sed 's/sudo //' ) \
  -f docker-compose.yml \
  $dev_config \
  -f docker-compose-build.yml \
  -f .env.yml \
  up --build --force-recreate --detach --remove-orphans

unshare_secrets

# Export SSL (if exists)
if [ ! -z $( $( get_docker_bin ) exec $(get_service_name app1) sh -c "if [ -d /etc/ssl/docker ]; then echo 1; fi" ) ]; then
  $( get_docker_bin ) cp $(get_service_name app1):/etc/ssl/docker/. docker-build/persistent/ssl/
fi

# Run before build scripts
for file in $( ls $dir/../../docker-build/build.d/post-build/*.sh ); do
  bash $file
done

echo
echo -en $formatSuccess
echo "#######################################"
echo "Build is done"
echo "#######################################"
echo -en $formatDefault
echo