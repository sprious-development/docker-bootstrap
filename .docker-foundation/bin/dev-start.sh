#!/bin/bash

# Exit when any command fails
set -e

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/lib/common.sh
cd $( realpath_shim $dir/../../ )

# Shim bridge networks on OSXs
if [ "osx" == "$(get_os_type)" ]; then
  bash $dir/util/mac-tuntap-up.sh
fi

echo -en $formatInfo
echo "Starting in dev mode..."
echo -en $formatDefault
$( get_docker_compose_bin ) \
  -f docker-compose.yml \
  -f docker-compose-dev.yml \
  -f .env.yml \
  up --detach