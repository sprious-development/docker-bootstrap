#!/bin/bash

# Exit when any command fails
set -e

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/../lib/common.sh
app_id=$(get_config_value COMPOSE_PROJECT_NAME)

$(get_docker_bin) exec -i $(get_service_name "${1:-app1}") sh <<-HEREDOC
apt-get install -y iputils-ping > /dev/null \
  && echo Host IP: \
  && ping -c1 host.docker.internal > /tmp/ping.log \
  && cat /tmp/ping.log \
  | head -n 1 \
  | awk '{print \$3}'\
  | sed 's/[()]//g'
HEREDOC