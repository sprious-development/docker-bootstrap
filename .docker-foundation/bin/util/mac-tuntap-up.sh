#!/bin/bash

# Exit when any command fails
set -e

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/../lib/common.sh

## Clone dependency
vendorDir=$dir/../vendor/docker-tuntap-osx
if [ ! -d "$vendorDir" ]; then
  git clone -q https://github.com/AlmirKadric-Published/docker-tuntap-osx.git "$vendorDir"
fi

# Install the driver, the first time it usually fails
if [ $(brew list | grep tuntap | wc -l ) == "0" ]; then
  echo "### Installing tun/tap driver..."
  brew install --cask tuntap
fi

# Install tun/tap if needed
if [ $(ifconfig | grep tap | wc -l ) == "0" ]; then
  bash "$vendorDir/sbin/docker_tap_install.sh"

  # Restart hypervisor if tap interface is not up yet
  if [ $(ifconfig | grep tap | wc -l ) == "0" ]; then
    bash "$vendorDir/sbin/docker_tap_install.sh" -f
    echo "Restarting..."

    # Wait until docker is up
    until [ $( $(get_docker_bin) ps | grep -i names | wc -l ) == "1" ]; do
      sleep 1
      echo "(waiting for docker.io)"
    done
  fi
fi

# Setup tap network interfaces
if [ $( ifconfig | grep 10.0.75.1 | wc -l ) == "0" ]; then
  echo "### Start network interfaces..."
  bash "$vendorDir/sbin/docker_tap_up.sh"
fi

# Route container subnet to tun/tap shim
subnet=$(get_config_value CONTAINER_SUBNET)
hostGateway="10.0.75.2"
if [ $(netstat -rn | grep "$subnet" | wc -l) = "0" ]; then
  echo "### Adding route container $subnet.x route..."
  sudo route -v add -net $subnet.1 -netmask 255.255.255.0 $hostGateway

  echo "---"
  echo "Congrats, tun/tap and bridge networks are up!"
fi
