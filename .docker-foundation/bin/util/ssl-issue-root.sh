#!/bin/bash

# Exit when any command fails
set -e

formatError="\033[0;31m"
formatSuccess="\033[1;32m"
formatInfo="\033[1;36m"
formatGray="\033[0;37m"
formatDefault="\033[0m"

# Dependencies & context
dir=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]:-$0}")" && pwd -P)
source $dir/../lib/common.sh
ssl_dir=$(realpath_shim "~/.docker-ssl")
if [ ! -d "$ssl_dir" ]; then
  mkdir -p "$ssl_dir"
fi
ssl_dir=$(realpath_shim "$ssl_dir" true) || ( echo $ssl_dir && exit 2 ); # make sure it exists or print error
userid=$( id -u $( whoami ) )

# Use nginx as it has openssl on the board
$(get_docker_bin) run -i -v $ssl_dir:/ssl nginx:stable bash <<-END
set -e

# Set context to export generated files
cd /ssl

# Check if we need to do the work
if [ -f "root.key" ] || [ -f "root.crt" ]; then
echo -e "
Either \"root.key\" or \"root.crt\" file exists in \"$ssl_dir\" what means root ssl was generated previously.
Use them or remove if you want to regenerate.

To use them, please use the following values in \".env\":
${formatInfo}
# SSL
CONTAINER_SSL_ROOT_PRIVATE=$ssl_dir/root.key
CONTAINER_SSL_ROOT_PUBLIC=$ssl_dir/root.crt
${formatDefault}"

  exit 1
fi

# root private key
openssl genrsa -out root.key 4096

# root public key
openssl req \
    -new \
    -key root.key \
    -newkey rsa:4096 \
    -pkeyopt ec_paramgen_curve:prime256v1 \
    -days 3650 \
    -nodes \
    -x509 \
    -subj "/C=../ST=...../L=..../O=@Docker Root CA/CN=Root CA ($( date '+%Y-%m-%d %H:%M:%S' ))/OU=And (and@appz.cloud)" \
    -out root.crt

# Set owner as generated certificates
chown $userid root.*
END

echo -e "
Congrats, root certificates have been generated! To use them, please update the following values in \".env\":
${formatInfo}
# SSL
CONTAINER_SSL_ROOT_PRIVATE=$ssl_dir/root.key
CONTAINER_SSL_ROOT_PUBLIC=$ssl_dir/root.crt
${formatDefault}"