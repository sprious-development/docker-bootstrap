#!/bin/bash

mkcert -install && echo "
Congrats, root certificates have been generated! To use them, please update the following values in \".env\":

CONTAINER_SSL_ROOT_PRIVATE=$( mkcert -CAROOT )/rootCA-key.pem
CONTAINER_SSL_ROOT_PUBLIC=$( mkcert -CAROOT )/rootCA.pem
"