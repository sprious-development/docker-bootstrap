#!/bin/bash

# Exit when any command fails
set -e

DOMAIN="$1"
if [ "$DOMAIN" = "" ]
then
  echo "Please provide domain name"
  exit 2
fi

mkdir /etc/ssl/docker
if [ -f "/secrets/ssl-root.key" ] && [ -f "/secrets/ssl-root.crt" ]; then

  # host private key
  openssl genrsa -out /etc/ssl/docker/host.key 4096

  # host sign request for host
  openssl req \
    -new \
    -key /etc/ssl/docker/host.key \
    -subj "/C=../ST=...../L=..../O=Self-signed certificate for Docker container/CN=*.$DOMAIN/OU=@Docker Root CA" \
    -out /etc/ssl/docker/host.csr

  # SAN config
  echo "authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $DOMAIN
DNS.2 = *.$DOMAIN
" > /etc/ssl/docker/host.ext

  # host public key with signed (OS X limitation 398 days, so use 365)
  openssl x509 \
    -req \
    -in /etc/ssl/docker/host.csr \
    -extfile /etc/ssl/docker/host.ext \
    -CAkey /secrets/ssl-root.key \
    -CA /secrets/ssl-root.crt \
    -CAcreateserial \
    -days 365 \
    -sha256 \
    -out /etc/ssl/docker/host.crt

# Fallback, generate self-signed certs without root certificate
else
  openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj \
    "/C=../ST=...../L=..../O=..../CN=..." \
    -keyout /etc/ssl/docker/host.key -out /etc/ssl/docker/host.crt
fi

# Install root certificate system wide
if [ -f "/secrets/ssl-root.crt" ]; then
  cp /secrets/ssl-root.crt /usr/local/share/ca-certificates/docker-root.crt
  update-ca-certificates
fi

# Set permissions to read for all processes
chmod ugo+r /etc/ssl/docker/host.*