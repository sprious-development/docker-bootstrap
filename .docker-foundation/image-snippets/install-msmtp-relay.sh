#!/bin/bash

# Exit when any command fails
set -e

SMTP_HOST="$1"
SMTP_DEFAULT_FROM="$2"
SMTP_PORT="$3"

if [ "$SMTP_HOST" = "" ]
then
  echo "SMTP host is empty: \"$SMTP_HOST\""
  exit 2
fi

if [ "SMTP_DEFAULT_FROM" = "" ]
then
  SMTP_DEFAULT_FROM="mailer@$( hostname )"
fi

if [ "SMTP_PORT" = "" ]
then
  SMTP_PORT=2500
fi

apt-get install -y --no-install-recommends mailutils msmtp msmtp-mta

echo "defaults

logfile /var/log/msmtp.log

# example with a gmail address
account default
host $SMTP_HOST
port $SMTP_PORT
from $SMTP_DEFAULT_FROM
" > /etc/msmtprc

# Support php
if [ -d /usr/local/etc/php/conf.d ]
then
  echo 'sendmail_path = "/usr/bin/msmtp -C /etc/msmtprc -t"' > /usr/local/etc/php/conf.d/msmtp-transport.ini
fi