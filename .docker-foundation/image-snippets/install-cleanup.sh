#!/bin/bash

# Exit when any command fails
set -e

if [ -x docker-php-source ]; then
  docker-php-source delete
fi

apt-get autoremove --purge -y \
  && apt-get autoclean -y \
  && apt-get clean -y \
  && rm -rf /usr/src