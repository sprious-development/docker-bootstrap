#!/bin/bash

# Exit when any command fails
set -e

rm /etc/nginx/conf.d/default.conf
sed -i -e 's/\/var\/log\/nginx\/error.log/\/dev\/stderr/g' /etc/nginx/nginx.conf
sed -i -e 's/\/var\/log\/nginx\/access.log/\/dev\/stdout/g' /etc/nginx/nginx.conf