#!/bin/bash

# Exit when any command fails
set -e

# Install the package
apt-get install -y --no-install-recommends supervisor

# Tune up the configs
sed -i "/^\[supervisord\].*/a user=root\nnodaemon=true" /etc/supervisor/supervisord.conf \
  && sed -i "s/^logfile=.*/logfile=\/dev\/null/" /etc/supervisor/supervisord.conf \
  && sed -i "s/^\(childlogdir=.*\)/; \1/" /etc/supervisor/supervisord.conf