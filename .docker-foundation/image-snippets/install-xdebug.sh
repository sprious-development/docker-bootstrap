#!/bin/bash

# Exit when any command fails
set -e

PHP_VERSION="$1"
if [ "$PHP_VERSION" = "" ]
then
  echo "Please provide PHP_VERSION"
  exit 2
fi

workaround() {
  version="$1"
  # https://stackoverflow.com/questions/69566437/no-releases-available-error-when-installing-pecl-in-docker
  apt-get install -y --no-install-recommends libcurl4-openssl-dev pkg-config libssl-dev git
  cd /tmp && \
     git clone https://github.com/xdebug/xdebug.git && \
     cd xdebug && \
     git checkout xdebug_$version && \
     phpize && \
     ./configure --enable-xdebug && \
     make && \
     make install && \
     rm -rf /tmp/xdebug
}

case "$PHP_VERSION" in
    5.6 | 7.0 | 7.1)
      # pecl install xdebug-2.5.0
      workaround "2_5"
      cp /tmp/php-conf.d/xdebug-v2.ini /usr/local/etc/php/conf.d/xdebug-v2.ini
      echo "php -d xdebug.remote_autostart=1 \"\$@\"" > /usr/local/bin/php-xdebug
      ;;
    7.2 | 7.3)
      pecl install xdebug-2.7.0
      cp /tmp/php-conf.d/xdebug-v2.ini /usr/local/etc/php/conf.d/xdebug-v2.ini
      echo "php -d xdebug.remote_autostart=1 \"\$@\"" > /usr/local/bin/php-xdebug
      ;;
    7.4)
        pecl install xdebug-3.0.1
        git checkout xdebug_3_0
        cp /tmp/php-conf.d/xdebug-v3.ini /usr/local/etc/php/conf.d/xdebug-v3.ini
        echo "php -d xdebug.start_with_request=yes -d xdebug.mode=debug \"\$@\"" > /usr/local/bin/php-xdebug
        ;;
    8.2)
      pecl install xdebug-3.2.2
      cp /tmp/php-conf.d/xdebug-v3.ini /usr/local/etc/php/conf.d/xdebug-v3.ini
      echo "php -d xdebug.start_with_request=yes -d xdebug.mode=debug \"\$@\"" > /usr/local/bin/php-xdebug
      ;;
    *)
      pecl install xdebug-3.1.3
      cp /tmp/php-conf.d/xdebug-v3.ini /usr/local/etc/php/conf.d/xdebug-v3.ini
      echo "php -d xdebug.start_with_request=yes -d xdebug.mode=debug \"\$@\"" > /usr/local/bin/php-xdebug
      ;;
  esac

docker-php-ext-enable xdebug
chmod a+x /usr/local/bin/php-xdebug

# Cleanup
rm -rf /tmp/php-conf.d