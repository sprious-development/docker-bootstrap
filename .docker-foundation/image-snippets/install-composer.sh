#!/bin/bash

# Exit when any command fails
set -e

PHP_VERSION="$1"
if [ "$PHP_VERSION" = "" ]
then
  echo "Please provide PHP_VERSION"
  exit 2
fi

apt-get update && apt-get install -y --no-install-recommends zip unzip wget git && docker-php-ext-install bcmath
wget https://getcomposer.org/installer -O - -q \
| php -- --install-dir=/bin --filename=composer $( \
  case "$PHP_VERSION" in \
      5.3 | 5.4 | 5.5 | 5.6 | 7.0 | 7.1) \
        echo --version=1.9.2 \
        ;; \
      *) \
        # Latest version
        ;; \
  esac \
    )
