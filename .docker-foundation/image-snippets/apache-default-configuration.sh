#!/bin/bash

# Exit when any command fails
set -e

HTTP_ROOT="$1"

a2dissite 000-default
a2enmod expires headers rewrite ssl remoteip
sed -e '/<Directory \/var\/www\/>/,/<\/Directory>/s/AllowOverride None/AllowOverride All/' -i /etc/apache2/apache2.conf

# Set DOCUMENT_ROOT
if [ "$HTTP_ROOT" != "" ]
then
  # Special value to make app root as http root
  if [ "$HTTP_ROOT" = "/" ]; then
    HTTP_ROOT=""
  fi
  sed -E "s/DocumentRoot [^\s]+/DocumentRoot \/app\/$HTTP_ROOT/g" -i /etc/apache2/sites-enabled/default.conf
fi