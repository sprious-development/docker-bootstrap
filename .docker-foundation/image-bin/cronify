#!/bin/bash

if [ "$1" == "--bin" ]; then
  MODE="bin"

  BIN="$2"
  ARG1="$3"
  ARG2="$4"
  ARG3="$5"
  ARG4="$6"
  ARG5="$7"
  BIN_PID=
else
  # Default mode
  MODE="service"

  DAEMON="$1"
  SERVICE_TO_STOP="$2"

  if [ ! -x "$(which $DAEMON)" ]; then
    echo "Binary \"$EXEC\" is not executable";
    exit 1
  fi

  if [ -z "$SERVICE_TO_STOP" ]; then
    echo "Please add 3rd parameter to ENTRYPOINT [\"cronify\", \"$DAEMON\", \"<service name (to stop)>\"]"
    exit 1
  fi

  if echo $( service $SERVICE_TO_STOP status 2>&1 ) | grep -q 'unrecognized'; then
    echo "Service name from ENTRYPOINT should be a valid service, but no \"$SERVICE_TO_STOP\" exists";
    service $SERVICE_TO_STOP status
    exit 1
  fi
fi

# based on https://github.com/renskiy/cron-docker-image/blob/master/debian/start-cron

# update default values of PAM environment variables (used by CRON scripts)
env | while read -r line; do  # read STDIN by line
    # split LINE by "="
    IFS="=" read var val <<< ${line}
    # remove existing definition of environment variable, ignoring exit code
    sed --in-place "/^${var}[[:blank:]=]/d" /etc/security/pam_env.conf || true
    # append new default value of environment variable
    echo "${var} DEFAULT=\"${val}\"" >> /etc/security/pam_env.conf
done

# PHP workaround, stops other crons after execution
mkdir -p /usr/cron-bin
if [ ! -z "$( which php )" ]; then
  echo "#!/bin/bash

/usr/local/bin/php -q \"\$@\" &
" > /usr/cron-bin/php && chmod a+x /usr/cron-bin/php
fi

rotateLog() {
  log="$1"
  maxLines=10000
  buffer=100 # allows exceed maxLines by this amount

  if [ ! -f "$log" ]; then
    echo "Log file does not exist \"$log\", and can not be rotated"
  fi

  while true; do
    lines="$(cat $log | wc -l)"

    # Keep last N lines
    if [ "$lines" -gt "$( expr $maxLines + $buffer )" ]; then
      echo "$(tail -n $maxLines $log)" > $log
    fi

    sleep 5
  done
}

# Write and listen logs
if [ ! -d "/var/log/cron" ]; then
  mkdir /var/log/cron
fi
# shellcheck disable=SC2045
for cron in /etc/cron.d/*; do
  cronPath="$cron"
  cron=$(basename "$cron")
   if [ "$cron" = ".placeholder" ] || [ "$cron" = "" ] || [ ! -z "$( cat $cronPath | grep WRAPPED )" ]; then
    continue
   fi

  logPath=/var/log/cron/"$cron".log

  # Add log output
  content="SHELL=/bin/bash
PATH=/usr/cron-bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
WRAPPED=yes
"
  while IFS= read -r line || [ -n "$line" ]; do
    # Skip empty lines
    if [ -z "$line" ]; then
      continue
    # Only if not output redirection applied
    elif echo "$line" | grep -qv '>'; then
      line="$line 2>&1 | awk -v date=\"\$(date --rfc-3339=seconds)\" '{print date \" [cron.$cron] \" \$0}' >> \"$logPath\" 2>&1"
    else
      # Use the defined log path
      logPath=$(echo "$line" | grep -Eo '[>]{1,2}[ "]*[^ "]+' | awk '{ print length, $0 }' | sort -nr | cut -d" " -f2- | sed -E 's/[> "]*//g' | head -n 1 | xargs)
      echo "Using defined log path \"$logPath\"..."
    fi
    content="$content$line
"
  done < "$cronPath"

  # Update cron config
  echo "$content" > "$cronPath"

  # Set permissions and listen cron logs
  touch "$logPath" \
    && chmod 777 "$logPath" \
    && {
      tail -n 0 -f "$logPath" &
      rotateLog "$logPath" &
      echo Listening \"$logPath\" cron log...;
    }

done

# Set correct permissions
chmod -R go-w /etc/cron.d && chmod 644 /etc/cron.d/*

service cron start || { echo "Error: Cron is not starting"; exit 1; }

## trap SIGINT and SIGTERM signals and gracefully exit
function shutdown() {
  echo Shutting down...;
  service cron stop;
  killall tail

  if [ "$MODE" == "service" ]; then
    service $SERVICE_TO_STOP stop
  elif [ "$MODE" == "bin" ]; then
    # Kill intelligently, by PID
    if [ ! -z "$BIN_PID" ]; then
      echo "Stopping $BIN (pid $BIN_PID)"
      kill -s "SIGTERM" $BIN_PID
    # Or by regexp if PID is not saved for some reason
    else
      killall -r "$BIN"
  fi

    echo Processes are stopped
  fi

  exit 0 # Clean exit
}
trap shutdown SIGINT
trap shutdown SIGTERM

# Start main daemon
if [ "$MODE" == "service" ]; then
  echo "Starting \"$DAEMON\""...
  eval "$DAEMON" & wait $!
elif [ "$MODE" == "bin" ]; then
  eval "$BIN $ARG1 $ARG2 $ARG3 $ARG4 $ARG5" & BIN_PID=$!;
  # Use last process pid
  wait $BIN_PID
fi
